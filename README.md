---
dataset_info:
  features:
  - name: result
    dtype: string
  - name: id
    dtype: int64
  splits:
  - name: train
    num_bytes: 166
    num_examples: 10
  download_size: 1324
  dataset_size: 166
configs:
- config_name: default
  data_files:
  - split: train
    path: data/train-*
---
# Dataset Card for "0c7ab6c4"

[More Information needed](https://github.com/huggingface/datasets/blob/main/CONTRIBUTING.md#how-to-contribute-to-the-dataset-cards)